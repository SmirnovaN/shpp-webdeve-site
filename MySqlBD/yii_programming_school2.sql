-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 21 2013 г., 19:23
-- Версия сервера: 5.5.32
-- Версия PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `yii_programming_school`
--
CREATE DATABASE IF NOT EXISTS `yii_programming_school` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `yii_programming_school`;

-- --------------------------------------------------------

--
-- Структура таблицы `advertisment`
--

CREATE TABLE IF NOT EXISTS `advertisment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `href` text CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `advertisment`
--

INSERT INTO `advertisment` (`id`, `href`, `text`) VALUES
(1, '//www.microsoft.com/uk-UA/default.aspx', 'Microsoft Corporation'),
(2, '//www.adobe.com/ua/', 'Adobe');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `text`) VALUES
(1, 'Один глаз, один рог, но не носорог?'),
(2, 'Кто приходит, кто уходит,\r\nВсе ее за ручку водят.'),
(3, 'Без языка живет,\r\nНе ест и не пьет,\r\nА говорит и поет.'),
(4, 'Хоть сама - и снег и лед,\r\nА уходит - слезы льет.'),
(14, 'wwe'),
(15, 'jihinjinjjn'),
(16, 'jjjjjjj');

-- --------------------------------------------------------

--
-- Структура таблицы `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `text` varchar(2000) NOT NULL,
  `image` varchar(200) NOT NULL,
  `link` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `courses`
--

INSERT INTO `courses` (`id`, `title`, `text`, `image`, `link`) VALUES
(1, 'Android', 'bla-bla-bla android it''s is very cool', 'images/android.jpg', 'android/index'),
(2, 'Web-developpment', 'bla-bla-bla Web-developpment it''s is very cool', 'images/web.jpg', ''),
(3, 'Детская группа', 'что-нибудь о детской группе что-нибудь о детской группе что-нибудь о детской группе что-нибудь о детской группе', 'images/baby.jpg', ''),
(4, 'Взрослая группа', 'что-нибудь о взрослой группе. что-нибудь о взрослой группе. что-нибудь о взрослой группе.  что-нибудь о взрослой группе.', 'images/girl-notebook.jpg', '');

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(50) DEFAULT NULL,
  `text` text,
  `image_url` char(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`id`, `title`, `text`, `image_url`, `date`) VALUES
(1, 'First event', 'Wonderfull text', 'images/web.jpg', '2013-11-01'),
(2, 'Second event', 'Interesting text', 'images/web.jpg', '2013-11-03'),
(3, 'third event', 'another interesting text', 'images/web.jpg', '2013-11-04'),
(4, 'Try to resize', '"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\r\n"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."', 'images/web.jpg', '2014-01-01'),
(5, ' Jabberwocky', 'Twas brillig, and the slithy toves Did gyre and gimble in the wabe; All mimsy were the borogoves, And the mome raths outgrabe. «Beware the Jabberwock, my son! The jaws that bite, the claws that catch! Beware the Jubjub bird, and shun The frumious Bandersnatch!» He took his vorpal sword in hand: Long time the manxome foe he sought — So rested he by the Tumtum tree, And stood awhile in thought. And as in uffish thought he stood, The Jabberwock, with eyes of flame, Came whiffling through the tulgey wood, And burbled as it came! One, two! One, two! and through and through The vorpal blade went snicker-snack! He left it dead, and with its head He went galumphing back. «And hast thou slain the Jabberwock? Come to my arms, my beamish boy! O frabjous day! Callooh! Callay!» He chortled in his joy. Twas brillig, and the slithy toves Did gyre and gimble in the wabe; All mimsy were the borogoves, And the mome raths outgrabe.', 'images/web.jpg', '2013-12-27'),
(6, 'adding line with migration', 'При работе с командой migrate рекомендуется использовать yiic приложения (то есть после cd path/to/protected), а не yiic из директории framework. Убедитесь, что директория protected\\migrations существует и доступна для записи. Также проверьте настройки соединения с базой данных в protected/config/console.php.Да именно в файле console.php должны быть указаны настройки к базе данных, а не protected/config/main.php.', 'images/web.jpg', '2013-10-01'),
(7, 'new line with migration', 'При работе с командой migrate рекомендуется использовать yiic приложения (то есть после cd path/to/protected), а не yiic из директории framework. Убедитесь, что директория protected\\migrations существует и доступна для записи. Также проверьте настройки соединения с базой данных в protected/config/console.php.Да именно в файле console.php должны быть указаны настройки к базе данных, а не protected/config/main.php.', 'images/web.jpg', '2013-11-11');

-- --------------------------------------------------------

--
-- Структура таблицы `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` text CHARACTER SET utf8 NOT NULL,
  `data` date NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `autor`, `data`, `text`) VALUES
(1, 'Michael', '2013-10-31', 'good school'),
(2, 'nastya', '2013-11-03', 'Nice!'),
(3, 'unknown', '2013-11-14', 'great!');

-- --------------------------------------------------------

--
-- Структура таблицы `library`
--

CREATE TABLE IF NOT EXISTS `library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `data_busy` date NOT NULL,
  `data_free` date NOT NULL,
  `user_book_busy` text NOT NULL,
  `book_free` int(11) NOT NULL,
  `info_book` text NOT NULL,
  `photo_href` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `library`
--

INSERT INTO `library` (`id`, `name`, `data_busy`, `data_free`, `user_book_busy`, `book_free`, `info_book`, `photo_href`) VALUES
(1, 'AJAX и PHP', '2013-12-17', '2013-12-25', 'Makc', 1, 'Книга "AJAX и PHP. Разработка динамических веб-приложений" - самый удобный и полезный ресурс, который поможет вам войти в захватывающий мир AJAX. Вы научитесь писать более эффективные веб-приложения на РНР за счет использования всего спектра возможностей технологий AJAX. Применение AJAX в связке с РНР и MySQL описывается на многочисленных примерах, которые читатель сможет использовать в собственных проектах. Рассмотрены следующие темы: верификация заполнения форм на стороне сервера; чат-приложение, основанное на технологии AJAX; реализация подсказок и функции автодополнения; построение диаграмм в реальном времени средствами SVG; настраиваемые и редактируемые таблицы на основе баз данных; реализация RSS-агрегатора; построение сортируемых списков с поддержкой механизма drag-and-drop.', 'a&p.jpg'),
(2, 'C++: базовый курс - Герберт Шилдт', '2013-12-17', '2013-12-17', 'Максим', 0, 'В этой книге описаны все основные средства языка С++ - от элементарных понятий до супервозможностей. После рассмотрения основ программирования на C++ (переменных, операторов, инструкций управления, функций, классов и объектов) читатель освоит такие более сложные средства языка, как механизм обработки исключительных ситуаций (исключений), шаблоны, пространства имен, динамическая идентификация типов, стандартная библиотека шаблонов (STL), а также познакомится с расширенным набором ключевых слов, используемым в .NET-программировании. Автор справочника - общепризнанный авторитет в области программирования на языках C и C++, Java и C# - включил в текст своей книги и советы программистам, которые позволят повысить эффективность их работы. Книга рассчитана на широкий круг читателей, желающих изучить язык программирования С++.', 'c++.jpg'),
(3, 'Adaptive Web Design', '2013-12-17', '2013-12-18', 'аноним', 1, 'Как и Adaptive Web Design, книга Макотта реально способна помочь вам в создании современного дизайна, а не укоренять вас в использовании методов вчерашнего дня. Ключевой идеей является сегодняшняя необходимость разрабатывать дизайн одновременно для мобильных браузеров, планшетов и нетбуков, равно как и для широких настольных мониторов. Книга описывает методы и принципы, лежащие в основе такой конструкции, как то: изменяющиеся сетки, подстраивающиеся изображения и media queries.', 'rwd.jpg'),
(4, 'Designing for the Digital Age', '2013-12-17', '2013-12-17', 'аноним', 1, 'Это не столько справочнки по веб-дизайну, сколько настольная книга обо всей отрасли в целом. Дизайн в нашу цифровую эпоху не может ограничиваться знаниями одной-двух специальностей.\r\nФриланс-консультант Лиза Райхельт считает ее просто обязательной к прочтению: \r\n«Учтите, что это не чтиво из серии „взять на пляж почитать“. Если у вас есть возможность потратить некоторое время дома на чтение, то стоит погрузиться в эту библию для дизайнеров».', 'dfda.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `title` text COLLATE utf8_bin NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `rutine` tinyint(1) NOT NULL,
  `preview` text COLLATE utf8_bin NOT NULL,
  `author` text COLLATE utf8_bin NOT NULL,
  `link` varchar(300) COLLATE utf8_bin NOT NULL,
  `image_main` varchar(100) COLLATE utf8_bin NOT NULL,
  `image_slider` varchar(300) COLLATE utf8_bin NOT NULL,
  `image_slider_thumbnail` varchar(300) COLLATE utf8_bin NOT NULL,
  `image_thumbnail` varchar(300) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `date`, `title`, `text`, `rutine`, `preview`, `author`, `link`, `image_main`, `image_slider`, `image_slider_thumbnail`, `image_thumbnail`) VALUES
(1, '2013-10-27', 'The first title', 'Main text information.', 1, 'Main text...', 'Johnny D', '', '', '../images/slider_images/1.jpg', '../images/slider_images/tn/1.jpg', '../images/1.png'),
(2, '2013-10-30', 'Ещё одна новость', 'Что-то здесь. Можно даже не читатьЧто-то здесь.\n\nЧто- зд QQQQWQW\nQW  Что-то здздесь.Что-то здздесь.Что-то зд QQQQWQWQW  Что-т\n здздесь.Ч\nто-то  зд QQQQWQWQW  Что-\nто здздесь.Что-то здзд зд QQQQWQWQW  Что-то здз\nдесь.Что-то здзд зд \nQQQQWQWQW  Что-то здздесь.Что-то здздздздесь.Что-то зд QQQQWQWQW  ', 0, 'Что-то здесь. Можно даже не читатьЧто-то здесь...', 'Степан', '', '', '../images/slider_images/2.jpg', '../images/slider_images/tn/2.jpg', '../images/2.png'),
(4, '2013-10-31', 'Еще и еще', 'фывдартфждафыдаоываофывд\nартфждафыдаоываофывдартфждафыдаоываофывдартфждафыдаоываофывдартфждафыдаоывао', 0, 'фывдартфждафыдаоываофывд...', '', '', '', '../images/slider_images/3.jpg', '../images/slider_images/tn/3.jpg', '../images/3.png'),
(5, '2013-10-23', 'какая-то новость №4', 'фывдартфждафыдаоыва210760193132107601931321076019313о34921076019313210760193135827421076019313', 1, 'фывдартфждафыдаоыва2...', '', '', '', '../images/slider_images/4.jpg', '../images/slider_images/tn/4.jpg', '../images/4.png'),
(8, '2013-10-01', 'Заголовок', 'дело было вечером, делать было нечего. дело было вечером, делать было нечего. дело было вечером, делать было нечего. дело было вечером, делать было нечего. дело было вечером, делать было нечего. дело было вечером, делать было нечего. дело было вечером, делать было нечего. ', 0, 'дело было вечером, делать было нечего...', 'Василий', '', '', '../images/slider_images/5.jpg', '../images/slider_images/tn/5.jpg', '../images/5.png'),
(9, '2013-10-22', 'testing news', 'some test for slider engine', 1, '', 'fsdf', '', '', '../images/slider_images/6.jpg', '../images/slider_images/tn/6.jpg', '../images/6.png'),
(10, '2013-11-01', 'News News News...', 'Сегодня отличный день все программируют', 0, 'фывдар', '', '', '', '../images/slider_images/7.jpg', '../images/slider_images/tn/7.jpg', '../images/7.png'),
(11, '2013-11-01', 'We are creating schools web-site', 'We are working about 7 days in a row without stopping. We do not hanging out with our friends. We do not watching movies or some serials. We are sitting here in front of our monitors and thinking, thinking, thinking...', 0, 'We are working about 7 days..', '', '', '', '../images/slider_images/8.jpg', '../images/slider_images/tn/8.jpg', '../images/8.png'),
(12, '2013-11-02', 'Some header', 'Придумайте сами. Надоело', 0, 'When I type git status, it shows all the changed files, including "disclosure_event.rb", under Changes to be committed. It shows no files as unstaged. I''ve repeatedly tried to add the file, but it seems to do nothing. If I open the file, everything looks great.', '', '', '/images/news/1.png', '../images/slider_images/9.jpg', '../images/slider_images/tn/9.jpg', '../images/9.png'),
(13, '2013-11-02', 'latest news', 'завершающий текст', 0, 'Здесь уже с псевдоэлементами:\nlink – определяет изначальный вид ссылки.\nvisited – определяет вид посещенной ссылки.\nhover – определяет вид ссылки в момент наведения на нее курсора.', '', '', '/images/news/pumpkin.png', '../images/slider_images/10.jpg', '../images/slider_images/tn/10.jpg', '../images/0.png');

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `defaultStatusComment` tinyint(1) NOT NULL,
  `defaultStatusUser` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`id`, `defaultStatusComment`, `defaultStatusUser`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1387380910),
('m131218_115348_insert_line_to_events', 1387380917),
('m131218_135742_testing_migration', 1387410286),
('m131218_152358_news', 1387380917),
('m131218_153913_news', 1387381618),
('m131218_155733_news_mig', 1387382677);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` int(11) NOT NULL,
  `ban` tinyint(1) unsigned NOT NULL,
  `role` tinyint(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_hash` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `created`, `ban`, `role`, `email`, `user_hash`) VALUES
(1, 'ivan', '4c09c792a3993da64e58e67bf6865c80', 0, 1, 0, '7ghjgh4535', '7uiy'),
(2, 'John', 'Snow', 0, 1, 1, '', ''),
(3, 'new', 'me', 0, 1, 2, '35rt', ''),
(7, 'bimbo', '0243b2274ee06217b3780c3ba270c031', 1387487837, 1, 0, '35345@gdfg4', ''),
(8, 'rew', '44c1826969ba20f6134e92fa7c945b03', 1387488329, 1, 0, '23', ''),
(9, 'crazycat', '9cc8d8f6657605e7432c2c995a6bf6f8', 1387488630, 1, 2, '343', ''),
(10, 'gordon', '3ea59c20a45805e8e8a50fe26f5791fd', 1387488916, 1, 1, '12@1.co', ''),
(11, 'bancheck', 'd06fb24f54ef4b32c21ab7abd7c49fcc', 1387490096, 1, 2, '1@1.rg', ''),
(12, 'bancheck', 'd06fb24f54ef4b32c21ab7abd7c49fcc', 1387490128, 1, 1, '1@1.rg', ''),
(13, 'ban', 'd06fb24f54ef4b32c21ab7abd7c49fcc', 1387490143, 0, 1, '123123@e.hjk', ''),
(14, '1', 'd06fb24f54ef4b32c21ab7abd7c49fcc', 1387491444, 0, 1, '1@1.rg', '');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text COLLATE utf8_bin NOT NULL,
  `password` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `md5` text COLLATE utf8_bin NOT NULL,
  `secret_word` text COLLATE utf8_bin NOT NULL,
  `online` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`, `md5`, `secret_word`, `online`, `admin`) VALUES
(1, 'nastya', 'Rfccbjgtyz', 'smth@mail.ru', '5b3563126c57638f247fd149fe738667', 'miracle', 1, 1),
(2, 'alex', '123', 'sjfksjf@jfsj.ru', '202cb962ac59075b964b07152d234b70', 'qwerty', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `voting`
--

CREATE TABLE IF NOT EXISTS `voting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voter` char(30) DEFAULT NULL,
  `voices` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `voting`
--

INSERT INTO `voting` (`id`, `voter`, `voices`) VALUES
(1, 'webdev', 29),
(2, 'android', 12),
(3, 'seo', 6),
(4, 'beer', 14),
(5, 'english', 10),
(6, 'gaming', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `we_are`
--

CREATE TABLE IF NOT EXISTS `we_are` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` text CHARACTER SET utf8 NOT NULL,
  `biography` text CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `we_are`
--

INSERT INTO `we_are` (`id`, `fio`, `biography`, `photo`) VALUES
(5, 'Рома Шмелев', 'фывдаыроал дрфар дфырдыфлар флар фдыраыф ыфлдарыфл рыфдра фл рдыфлар фыдра фыдлры', 'images/about_us/shmelev.png'),
(6, 'Книшук Анатолий Василиевич', 'ФЫДЛР ЛДАР ДЫЛРАДЫВАР Лфдлыра длфыр алдфыра длфырал рядчмяю бчтнг нрдыфтлбф \r\n', 'images/about_us/knishuk.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
