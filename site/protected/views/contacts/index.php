<?php
/* @var $this ContatcsController */

$this->breadcrumbs=array(
	'Contatcs',
);
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/contacts.js"></script>
<div id = "contact-info">
    <p><b>Наши контакты:</b></p>

    <img src = "<?php echo Yii::app()->request->baseUrl; ?>/images/letter.png">&nbsp<a href="mailto:info@programming.kr.ua">info@programming.kr.ua</a></img><br />
    <img src = "<?php echo Yii::app()->request->baseUrl; ?>/images/phone.png">&nbsp 099 196 24 69 <a href="<?php echo Yii::app()->request->baseUrl; ?>/about_us">Роман</a></img><br/ >
    <br />
    <p><b>Наш адрес:</b></p>
    <p>г. Кировоград, ул. Тимирязева 77</p>
    <br /><br />
    А лучше всего, встретится у нас, выпить чаю,<br />
    кусануть печеньку, и сделать правильный вывод :)
    <br /><br />



</div>
<div id = "contact-form">
    <p><b>Задайте нам свой вопрос: </b></p>
    <div>
        <textarea rows = "7" cols = "70" name = "mail_message" id = "message"></textarea>
        <br />
        <div class="capcha">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/js/other_scripts/captcha.php" />
            <br />
            <input type="text" id="capcha_value" />
        </div>
        <br />

        <button id = "mail_send" onclick="bot_check();">Прочитайте мое письмо пожалуйста ...</button>
    </div>
    <span id = "notice">*лучше укажите в сообщении свой e-mail , чтобы мы могли вам ответить </span>
    <br><br>
    <p><b>Мы на карте:</b></p>
    <br>
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;t=h&amp;source=embed&amp;ll=48.507006,32.270762&amp;spn=0.001244,0.00228&amp;z=18&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/?ie=UTF8&amp;t=h&amp;source=embed&amp;ll=48.507006,32.270762&amp;spn=0.001244,0.00228&amp;z=18" style="color:#0000FF;text-align:left">Просмотреть увеличенную карту</a></small>
</div>
<br clear="all"/>