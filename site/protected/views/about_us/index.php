<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/about_us.js"></script>
<div id="about_us" xmlns="http://www.w3.org/1999/html">
    <div id="au_top">
        <button id="weare">Нас больше</button>
    </div>
    <div id="au_much_more" class="hide_div">
        <?php
        foreach ($data[2][0] as $value)
        {
            $new_html =  '<b>' . $value['fio'] . ': </b></br><img class="photo_teacher" src="'.Yii::app()->request->baseUrl."/".$value['photo'].'"><p>' . $value['biography'] . '</p></br>';
            echo $new_html;
        }
        ?>
    </div>
    <div id="au_maininfo">
        <div id="au_maininfo_text">
            <p>Школа создана с целью вывести обучение программированию в Кировограде на новый уровень и способствовать развитию ИТ-сообщества в городе.
                Фокусируемся в первую очередь именно на программировании - "элитной" области ИТ.
                Работает как взрослая, так и детская группы.</p>
            <p>Целью обучения взрослой группы является подготовка человека “с нуля” за короткий срок к уверенному началу своей карьеры junior-developer'ом, умению качественно и продуктивно выполнять поставленные реальные задачи. Соответственно ориентируемся на те дисциплины, которые актуальны на данный момент.</p>
            <p>Школа постоянно развивается в плане технического обеспечения, методической базы.
                Мы сотрудничаем с различными фирмами, что позволяет практически гарантировать трудоустройство в случае успешного окончания обучения в нашей школе, а также это означает проведение лекций преподавателями-практиками, которые имеют опыт зарабатывания денег в своей области и понимание актуальности той или иной технологии.</p>
            <p>Высокий уровень заработка программиста позволяет быстро компенсировать затраты на обучение у нас.</p>
            <p>Срок обучения взрослой группы - 6 месяцев.<p/>
            <p>На данный момент уже работает группа по web development (HTML/CSS/JS/frameworks + PHP/MySQL/CMS). В скором времени стартует группа по Android (mobile development).
                Также продолжается набор в детские группы.</p>
            <p>Школа создана энтузиастами, мы ориентируемся на качество, а не количество, и не собираемся останавливаться на достигнутом :)</p>
        </div>
        <div id="au_maininfo_menu">
            <center>
                <span id="au_mainmenu_advertisment" class="active">Реклама/</span>
                <span id="au_mainmenu_feedbacks" class="active">\Отзывы</span>
            </center>
            <div id="au_mainmenu_content">
                <div id="au_advertisment">
                    <?php
                    foreach ($data[0][0] as $value)
                    {
                        $new_html = '<a href="' . $value['href'] . '">' . $value['text'] . '</a></br>';
                        echo $new_html;
                    }
                    ?>
                </div>
                <div id="au_feedbacks" class="hide_div">
                    <?php
                    foreach ($data[1][0] as $value)
                    {
                        $new_html =  '<b>' . $value['autor'] . ': </b><p>' . $value['text'] . '</p></br>';
                        echo $new_html;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
