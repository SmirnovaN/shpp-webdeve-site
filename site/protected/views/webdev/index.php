﻿<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/course.js"></script>
<div class="content-course">
		<div class="preview-test">
			<div class="webdev-img">
				<img src="images/webdev.jpg" alt="Web development"/>
			</div>
			<div class="test-list">
				<center><h3>Проверь свои знания!</h3></center>
				<br/>
				<ul>
					<li class="clicked-link">HTML</li>
					<li class="clicked-link">CSS</li>
					<li class="clicked-link">JavaScript</a></li>
					<li class="clicked-link">jQuery</li>
					<li class="clicked-link">PHP</li>
					<li class="clicked-link">MySQL</li>
				</ul>
				<center><button>Хотелось бы пройти курс..</button></center>
			</div>
			<div class="peoplelist">
			<center><h4>Вместе с Вами тест на знание <span id="test-name"></span> ожидают:</h4></center>
			<div class="online-list"></div>
			<!--List of online users from DataBase-->
			<center><h5 id="timetext">Дождитесь разрешения администратора через:</h5><div id="countdown">00:05</div></center>
			</div>
		</div>
		<div class="test">
			<div id="first-question" class="question-div">
				<p class="question" ><b>1.&nbsp;What does HTML stand for?</b></p>        
				<ul class="answers">            
				<input type="radio" name="q1" value="1" id="q1a"><label for="q1a">Hyper Text Markup Language</label><br/>          
				<input type="radio" name="q1" value="0" id="q1b"><label for="q1b">Home Tool Markup Language</label><br/>            
				<input type="radio" name="q1" value="0" id="q1c"><label for="q1c">Hyperlinks and Text Markup Language</label><br/>            
				</ul> 
				<br/>
				<div id="next1">            
					Next question
				</div> 
			</div>
			<div id="second-question" class="question-div">
				<p class="question"><b>2.&nbsp;Who is making the Web standards?</b></p>        
				<ul class="answers">            
				<input type="radio" name="q2" value="0" id="q2a"><label for="q2a">Microsoft</label><br/>           
				<input type="radio" name="q2" value="1" id="q2b"><label for="q2b">The World Wide Web Consortium</label><br/>
				<input type="radio" name="q2" value="0" id="q2c"><label for="q2c">Mozilla</label><br/>           
				<input type="radio" name="q2" value="0" id="q2d"><label for="q2d">Google</label><br/>       
				</ul>        
				<br/>
				<div id="next2">            
					Next question
				</div>
			</div>
			<div id="third-question" class="question-div">
				<p class="question"><b>3.&nbsp;Choose the correct HTML tag for the largest heading</b></p>        
				<ul class="answers">            
				<input type="radio" name="q3" value="1" id="q3a"><label for="q3a">&lt;h1&gt;</label><br/>           
				<input type="radio" name="q3" value="0" id="q3b"><label for="q3b">&lt;h6&gt;</label><br/>
				<input type="radio" name="q3" value="0" id="q3c"><label for="q3c">&lt;heading&gt;</label><br/>           
				<input type="radio" name="q3" value="0" id="q3d"><label for="q3d">&lt;head&gt;</label><br/>       
				</ul>        
				<br/>
				<div id="results">            
					Show me the results! 
				</div>
			</div>
			<div class="score">            
				<p>Congradulations! You answered:&nbsp;<strong><span id="scored"></span></strong>&nbsp;questions correctly!</p>        
			</div>        

		</div>
		
		<div class="description">
			<p>Если говорить о web dev , то мы учим итерациями по 2 месяца - за 3 итерации человек обучается выбранному направлению. По окончании каждой итерации ученик имеет «полный» спектр знаний. На следующих итерациях знания объединяются между собой, заполняются пробелы, на построенной базе выходим на более практические навыки.</p>
			<p>Обычно занятие состоит из двух уроков (каждый урок = 1 час + возможно 5-10 минут перерыва).</p>
			<p>В промежутке между уроками кушаем печеньки, пьем чай, бегаем в туалет, доделываем что-то.</p>
			<p>Наши ученики - взрослые люди со своим опытом и своим мнением о том, что им следует брать у нас, как брать, в каком ритме двигаться, какие домашние задания делать, но...</p>
			<p>Мы просим - постарайтесь выбросить все это и слушать нас, ведь вы выбрали нас чтобы мы вас привели к вашей цели - слушайтесь нас! Даже если мы требуем "невозможного" - это такой метод обучения, делайте то что мы говорим, делайте невозможное.</p>
		</div>
	</div>


