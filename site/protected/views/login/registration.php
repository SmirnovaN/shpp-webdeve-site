<?php if (Yii::app()->user->hasFlash('registration')): ?>

<div class ="flash-success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>

<?php else: ?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="labelwidth"><?php echo $form->labelEx($model,'username'); ?></div>
        <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
        <?php /*echo $form->error($model,'username'); */?>
    </div>

    <div class="row">
        <div class="labelwidth"><?php echo $form->labelEx($model,'password'); ?></div>
        <?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
        <?php /*echo $form->error($model,'password'); */?>
    </div>

    <div class="row">
        <div class="labelwidth"><?php echo $form->labelEx($model,'email'); ?></div>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
        <?php /*echo $form->error($model,'email'); */?>
    </div>



    <div class="buttons">
        <?php echo CHtml::submitButton('Регистрация'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php endif; ?>