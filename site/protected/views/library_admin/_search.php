<?php
/* @var $this Library_adminController */
/* @var $model Library_admin */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textArea($model,'name',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_busy'); ?>
		<?php echo $form->textField($model,'data_busy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_free'); ?>
		<?php echo $form->textField($model,'data_free'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_book_busy'); ?>
		<?php echo $form->textArea($model,'user_book_busy',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'book_free'); ?>
		<?php echo $form->textField($model,'book_free'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'info_book'); ?>
		<?php echo $form->textArea($model,'info_book',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'photo_href'); ?>
		<?php echo $form->textArea($model,'photo_href',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->