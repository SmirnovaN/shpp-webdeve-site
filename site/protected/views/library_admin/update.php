<?php
/* @var $this Library_adminController */
/* @var $model Library_admin */

$this->breadcrumbs=array(
	'Library Admins'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Library_admin', 'url'=>array('index')),
	array('label'=>'Create Library_admin', 'url'=>array('create')),
	array('label'=>'View Library_admin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Library_admin', 'url'=>array('admin')),
);
?>

<h1>Update Library_admin <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>