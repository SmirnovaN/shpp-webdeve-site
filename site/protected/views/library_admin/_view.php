<?php
/* @var $this Library_adminController */
/* @var $data Library_admin */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_busy')); ?>:</b>
	<?php echo CHtml::encode($data->data_busy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_free')); ?>:</b>
	<?php echo CHtml::encode($data->data_free); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_book_busy')); ?>:</b>
	<?php echo CHtml::encode($data->user_book_busy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('book_free')); ?>:</b>
	<?php echo CHtml::encode($data->book_free); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info_book')); ?>:</b>
	<?php echo CHtml::encode($data->info_book); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('photo_href')); ?>:</b>
	<?php echo CHtml::encode($data->photo_href); ?>
	<br />

	*/ ?>

</div>