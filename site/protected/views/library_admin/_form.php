<?php
/* @var $this Library_adminController */
/* @var $model Library_admin */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'library-admin-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textArea($model,'name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_busy'); ?>
		<?php echo $form->textField($model,'data_busy'); ?>
		<?php echo $form->error($model,'data_busy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_free'); ?>
		<?php echo $form->textField($model,'data_free'); ?>
		<?php echo $form->error($model,'data_free'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_book_busy'); ?>
		<?php echo $form->textArea($model,'user_book_busy',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'user_book_busy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'book_free'); ?>
		<?php echo $form->textField($model,'book_free'); ?>
		<?php echo $form->error($model,'book_free'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'info_book'); ?>
		<?php echo $form->textArea($model,'info_book',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'info_book'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'photo_href'); ?>
		<?php echo $form->textArea($model,'photo_href',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'photo_href'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->