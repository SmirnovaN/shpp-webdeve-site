<?php
/* @var $this Library_adminController */
/* @var $model Library_admin */

$this->breadcrumbs=array(
	'Library Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Library_admin', 'url'=>array('index')),
	array('label'=>'Manage Library_admin', 'url'=>array('admin')),
);
?>

<h1>Create Library_admin</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>