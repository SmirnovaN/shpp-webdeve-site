<?php
/* @var $this Library_adminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Library Admins',
);

$this->menu=array(
	array('label'=>'Create Library_admin', 'url'=>array('create')),
	array('label'=>'Manage Library_admin', 'url'=>array('admin')),
);
?>

<h1>Library Admins</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
