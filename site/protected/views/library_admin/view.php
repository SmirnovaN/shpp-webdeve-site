<?php
/* @var $this Library_adminController */
/* @var $model Library_admin */

$this->breadcrumbs=array(
	'Library Admins'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Library_admin', 'url'=>array('index')),
	array('label'=>'Create Library_admin', 'url'=>array('create')),
	array('label'=>'Update Library_admin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Library_admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Library_admin', 'url'=>array('admin')),
);
?>

<h1>View Library_admin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'data_busy',
		'data_free',
		'user_book_busy',
		'book_free',
		'info_book',
		'photo_href',
	),
)); ?>
