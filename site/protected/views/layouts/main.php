﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/admins_check.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/toTopButton.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/home.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/news.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/about_us.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/contact.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/course_test.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/voting.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/android.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/events.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/library.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/Login.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/adminUser.css">
    <title>Школа программирования Ш++</title>
</head>
<body>
    <div id="wrap">
        <div id="header">
            <a href="<?php echo Yii::app()->request->baseUrl; ?>"><img id="header_main_logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/template/ball.png" alt="временный логотип :)" title="Happy New Year!"/></a>
            <ul class="main_menu">
				<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/Home')),
				array('label'=>'News', 'url'=>array('/News')),
				array('label'=>'About_Us', 'url'=>array('/About_us')),
				array('label'=>'Contacts', 'url'=>array('/Contacts')),
				array('label'=>'Library', 'url'=>array('/Library')),
				array('label'=>'Events', 'url'=>array('/Events')),
                array('label'=>'Пользователи', 'url'=>array('/admin/user'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Настройка', 'url'=>array('/admin/setting'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Библиотека', 'url'=>array('/library_admin'), 'visible'=>!Yii::app()->user->isGuest),			),
		)); ?>
               <?php if(isset($this->breadcrumbs)):?>
			<?php /*$this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			)); */?><!-- breadcrumbs -->
            </ul>
            <div id="header_login">
                <input id="header_login_username" type="text">
                <input id="header_login_password" type="password">
                <button id="header_login_dolog" onclick='location.href = "<?php echo Yii::app()->getHomeUrl().'Login'; ?>"'>Login</button>

                
                <a href='<?php echo Yii::app()->getHomeUrl()."Registration"?>'>Registration</a>
                <?php $this->widget('zii.widgets.CMenu',array(
                    'items'=>array(
                        array('label'=>'Логин', 'url'=>array('/login/index'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Регистрация', 'url'=>array('login/registration'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/login/logout'), 'visible'=>!Yii::app()->user->isGuest)


                    ),
                ));
                if(Yii::app()->user->checkAccess('2')){
                    echo "hello, I'm administrator(Вставить сюда ссылку на страницы администрирования)";
                }

                ?>

            </div>
        </div>
        <div id="content">
			
			<?php endif ?>

			<?php echo $content; ?>

        </div>
        <div id="footer">
            <ul class="main_menu">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'items'=>array(
                        array('label'=>'Home', 'url'=>array('/Home')),
                        array('label'=>'News', 'url'=>array('/News')),
                        array('label'=>'About_Us', 'url'=>array('/About_us')),
                        array('label'=>'Contacts', 'url'=>array('/Contacts')),
                        array('label'=>'Library', 'url'=>array('/Library')),
                        array('label'=>'Events', 'url'=>array('/Events'))
                    ),
                )); ?>
            </ul>
            <div class="social">
            <a href="http://vk.com/programming_in_kr"><img id="Vkontakte" title="Вконтакте" onmouseover="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/v-light.jpg ';" onmouseout="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/v.jpg ';" src="<?php echo Yii::app()->request->baseUrl; ?>/images/social/v.jpg" alt="Вконтакте" /></a>
            <a href="#ссылка"><img id="Facebook" title="Facebook" onmouseover="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/f-light.jpg ';" onmouseout="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/f.jpg ';" src="<?php echo Yii::app()->request->baseUrl; ?>/images/social/f.jpg" alt="Facebook" /></a>
            <a href="#ссылка"><img id="GooglePlus" title="GooglePlus" onmouseover="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/g-light.jpg ';" onmouseout="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/g.jpg ';" src="<?php echo Yii::app()->request->baseUrl; ?>/images/social/g.jpg" alt="GooglePlus" /></a>
            <a href="#ссылка"><img id="Twitter" title="Twitter" onmouseover="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/t-light.jpg ';" onmouseout="this.src='<?php echo Yii::app()->request->baseUrl; ?>/images/social/t.jpg ';" src="<?php echo Yii::app()->request->baseUrl; ?>/images/social/t.jpg" alt="Twitter" /></a>
           </div>
            <div class="footerText"><p>&copy; 2012-2014 <a href="<?php echo Yii::app()->request->baseUrl; ?>">Ш++</a></p><p>Использование материалов сайта разрешено только при условии размещения ссылки на источник</p></div>
            <p id="back-top">
                <a href="#top"><span></span>Вверх</a>
            </p>
        </div>
    </div>
</body>
</html>