<!--the loading screen part-->

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/android.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tooltip.js"></script>
<div id="andriod_phone"><em>slide to unlock<i></i></em>
    <div id ="andriod_phone_screen">
        <div id="andriod_phone_screen_slider">
            <div id="unlock"></div>
        </div>
    </div>
</div>
<div class="slideToUnlock"></div>
<div id="bg_layer"></div>

<!--the loading screen part-->

<div class="android_description">
    <div class="android_headers">Cистема</div>
    <p>
        <a href="http://www.android.com/">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/android/logo.png"></img>
        </a> — операционная система для смартфонов, планшетных компьютеров, электронных книг, цифровых проигрывателей, наручных часов, нетбуков и смартбуков и других устройств, основанная на ядре Linux и собственной реализации Java от Google.
        В 79,3 % смартфонов, проданных во втором квартале 2013 года, была установлена операционная система Android
    </p>
    <div class="android_headers">Школа</div>
    <p>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>">Кировоградская школа программирования</a> предлагает вам присоединиться к одному из самых перспективных и наиболее быстро развивающихся направлений развития IT-технологий.
        У нас вы сможете получить все необходимые знания для свободного создания собственных приложений под операционную систему Android
    </p>
    <div class="android_headers">Результат</div>
    <span class="android_results">После прохождения курсов вы сможете разрабатывать любые приложения для этой системы, в том числе:</span>
    <div class="android_apps">
        <div class="android_left">
            <div class="android_headers">Программы переднего плана</div>
            Приложения работающие, когда они видимы на экране, в противном случае их выполнение приостанавливается. Пример — игры.
        </div>
        <div class="android_right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/android/games.png"></img></div>
    </div>
    <div class="android_apps">
        <div class="android_left">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/android/sms.png"></img>
        </div>
        <div class="android_right"><div class="android_headers">Фоновые</div>
        Приложения, с которыми  пользователи практически не взаимодействуют, за исключением  их настройки.  Большую  часть времени они находятся  в скрытом состоянии. Пример — службы экранирования звонков и SMS-автоответчики.</div>
    </div>
    <div class="android_apps">
        <div class="android_left">
            <div class="android_headers">Смешанные</div>
            Предполагают некоторую  степень  интерактивности, однако большую часть времени  работают в фоновом  режиме.  Как правило, после настройки незаметны. Лишь при необходимости уве- домляют пользователя о каких-либо событиях. Пример — мультимедийный проигрыватель.
        </div>
        <div class="android_right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/android/mediaplayer.png"></img></div>
    </div>
    <div class="android_apps">
        <div class="android_left">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/android/widgets.png"></img>
        </div>
        <div class="android_right">
            <div class="android_headers">Виджеты</div>
            Некоторые приложения представлены исключительно в виде виджетов, размещаемых  на домашнем экране.
        </div>
    </div>
    <p>всё это и многое другое вы научитесь делать в кратчайшие сроки, с высоким уровнем скорости и качества</p>
    <div class="android_headers">Обучение</div>
    <p>Период обучения длится от 6 до 12 месяцев (в зависимости от ваших пожеланий). Занятия разбиты на 3 или 6 секций по 2 месяца каждый. В конце каждой секции проводится тестирование уровня знаний участников. Каждое занятие для вашего удобства записывается на видео и предоставляется слушателям.</p>
    <div class="android_headers">Дополнительная информация</div>
    <p>Вся дополнительная информация в т.ч. стоимость время и место проведения занятий согласовывается в личной беседе с клиентом. Узнать наши контактные данные, а также задать все интересующие вас вопросы вы можете
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/Contacts/index">тут</a>
    </p>
</div>