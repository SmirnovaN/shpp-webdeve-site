<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $user_login
 * @property string $user_password
 * @property string $user_hash
 * @property integer $user_ip
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_login, user_password, user_hash, user_ip', 'required'),
			array('user_ip', 'numerical', 'integerOnly'=>true),
			array('user_login', 'length', 'max'=>30),
			array('user_password, user_hash', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, user_login, user_password, user_hash, user_ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'user_login' => 'User Login',
			'user_password' => 'User Password',
			'user_hash' => 'User Hash',
			'user_ip' => 'User Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_login',$this->user_login,true);
		$criteria->compare('user_password',$this->user_password,true);
		$criteria->compare('user_hash',$this->user_hash,true);
		$criteria->compare('user_ip',$this->user_ip);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function checkUser($login){
            

            $criteria = new CDbCriteria;
            $criteria->select =  array(
                'COUNT(`user_id`)'
             );
            $criteria->condition = "user_login ='".$login."'";
            $criteria->select =  'user_id';
            $criteria->condition = "user_login = '".$login."'";
            $result = User::model()->findAll($criteria);
            
            return $result[0]['user_id'];

            
            $text = User::model()->findBySql('SELECT user_login FROM user WHERE user_login='.$login.'');
            
            $count = count($text);
            
            return $count;

        }
        
        public function registerUser($login,$password,$email){
            
        }
}
