<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $date
 * @property string $title
 * @property string $text
 * @property integer $rutine
 * @property string $preview
 * @property string $author
 * @property string $link
 * @property string $image_main
 * @property string $image_slider
 * @property string $image_slider_thumbnail
 * @property string $image_thumbnail
 */
class News_home extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, title, text, rutine, preview, author, link, image_main, image_slider, image_slider_thumbnail, image_thumbnail', 'required'),
			array('rutine', 'numerical', 'integerOnly'=>true),
			array('link, image_slider, image_slider_thumbnail, image_thumbnail', 'length', 'max'=>300),
			array('image_main', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, title, text, rutine, preview, author, link, image_main, image_slider, image_slider_thumbnail, image_thumbnail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'title' => 'Title',
			'text' => 'Text',
			'rutine' => 'Rutine',
			'preview' => 'Preview',
			'author' => 'Author',
			'link' => 'Link',
			'image_main' => 'Image Main',
			'image_slider' => 'Image Slider',
			'image_slider_thumbnail' => 'Image Slider Thumbnail',
			'image_thumbnail' => 'Image Thumbnail',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('rutine',$this->rutine);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('image_main',$this->image_main,true);
		$criteria->compare('image_slider',$this->image_slider,true);
		$criteria->compare('image_slider_thumbnail',$this->image_slider_thumbnail,true);
		$criteria->compare('image_thumbnail',$this->image_thumbnail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News_home the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getList(){
		$criteria = new CDbCriteria;
		$criteria->select = '`title`,`date`,`id`,`image_main`,`preview`';
		$criteria->order = 'date DESC';
		
		$a = News_home::model()->findAll($criteria);
		
		$array_info = array();
		$data_array = array();
		
		foreach($a as $one){
			$array_info['title'] = $one->title;
                        $array_info['preview'] = $one->preview;
			$array_info['date'] = $one->date;
			$array_info['id'] = $one->id;
			$array_info['image_main'] = $one->image_main;
			$array_info['image_thumbnail'] = $one->image_thumbnail;
			array_push($data_array,$array_info);
		}
		
		return $data_array;	
	}
	
	 public function get_AllInfo($argument){
		$new_argument =  json_decode($argument);
		
		$criteria = new CDbCriteria;
		$criteria->select = '`text`,`image_main';
		$criteria->condition ='id='.$argument;
		
		$a = News_home::model()->findAll($criteria);
		
		$array_info = array();
		$data_array = array();
		
		foreach($a as $one){
			$array_info["image_main"] = $one->image_main;
			$array_info["text"] = $one->text;
			array_push($data_array,$array_info);
		}
		return json_encode($data_array);
	 }
     
     public function FullPage($argument){
         
         $criteria = new CDbCriteria;
         $criteria->condition ='id='.$argument;
         
         $a = News_home::model()->findAll($criteria);
         
         $array_info = array();
		 $data_array = array();
         
         foreach($a as $one){
			$array_info["date"] = $one->date;
			$array_info["text"] = $one->text;
            $array_info["image_main"] = $one->image_main;
            $array_info["author"] = $one->author;
            $array_info["title"] = $one->title;
			array_push($data_array,$array_info);
		}
        return $data_array;
     }
}
