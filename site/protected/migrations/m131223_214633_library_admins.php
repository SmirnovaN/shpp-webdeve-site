<?php

class m131223_214633_library_admins extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('library','popular_book','tinyint');
        $this->addColumn('library','luxury_book','tinyint');
    }
    
    public function safeDown()
    {
        echo "m131223_214633_library_admins failed.\n";
        return false;
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}