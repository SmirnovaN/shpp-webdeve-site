<?php

class About_usController extends Controller
{
	public function actionIndex()
	{
        $data_array = array();

        /*выборка из таблицы рекламы*/

        $criteria = new CDbCriteria;
        $criteria->limit='10';
        $criteria->order='id DESC';
        $a = Advertisment::model()->findAll($criteria);

        $array_advert=array();
        $arr_advert=array();
        foreach($a as $item)
        {
            $array_advert["href"]=$item->href;
            $array_advert["text"]=$item->text;
            array_push($arr_advert,$array_advert);
        }

        /*выборка из таблицы отзывов*/

        $criteria = new CDbCriteria;
        $criteria->limit='10';
        $criteria->order='id DESC';
        $a = Feedbacks::model()->findAll($criteria);
        $array_feed = array();
        $arr_feed = array();
        foreach($a as $item)
        {
            $array_feed["autor"]=$item->autor;
            $array_feed["text"]=$item->text;
            array_push($arr_feed,$array_feed);
        }

        /*выборка из таблицы о сотрудниках*/

        $criteria = new CDbCriteria;
        $criteria->limit='10';
        $criteria->order='id DESC';
        $a = WeAre::model()->findAll($criteria);
        $array_info = array();
        $arr_info = array();
        foreach($a as $item)
        {
            $array_info["fio"]=$item->fio;
            $array_info["biography"]=$item->biography;
            $array_info["photo"]=$item->photo;
            array_push($arr_info,$array_info);
        }

        /*сборка в общий массив и его вывод*/

        array_push($data_array,$arr_advert,$arr_feed,$arr_info);

        $resulting_array = array(
            array($data_array[0]),     // advertisment
            array($data_array[1]),     // feedbacks
            array($data_array[2])      // personal info
        );

        $this->render('index',array('data'=>$resulting_array));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}