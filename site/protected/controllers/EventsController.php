﻿<?php

class EventsController extends Controller
{
	public function actionFuture()
	{
		$date=date("Y-m-d");
		$criteria = new CDbCriteria;
		$criteria->limit='4';
		$criteria->order='date ASC';
		$criteria->condition='date > :date';
		$criteria->params=array(':date' => $date);
		$a = Events::model()->findAll($criteria);
		$array=array();
		$arr=array();
		foreach($a as $item)
		{
			$array["id"]=$item->id;
			$array["title"]=$item->title;
			$array["text"]=$item->text;
			$array["text"]=substr($array["text"], 0, 200);
			$array["date"]=$item->date;
			$array["image_url"]=$item->image_url;
			array_push($arr,$array);		
		}
		$j=CJSON::encode($arr);
		echo $j;
	}
	public function actionFull()
	{
		$id=$_POST['id'];
		$text = Events::model()->findBySql('SELECT text FROM events WHERE id='.$id.'');
		echo $text->text;
	}
	public function actionCut()
	{
		$id=$_POST['id'];
		$text = Events::model()->findBySql('SELECT text FROM events WHERE id='.$id.'');
		$cuttext=$text->text;
		$cuttext=substr($cuttext, 0, 200);
		echo $cuttext;
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionNearfuture()
	{
		$date=date("Y-m-d");
		$criteria = new CDbCriteria;
		$criteria->order='date ASC';
		$criteria->limit='1';
		$criteria->condition='date > :date';
		$criteria->params=array(':date' => $date);
		$a = Events::model()->findAll($criteria);
		$array=array();
		$arr=array();
		foreach($a as $item)
		{
			$array["title"]=$item->title;
			$array["text"]=$item->text;
			$array["date"]=$item->date;
			$array["image_url"]=$item->image_url;
			array_push($arr,$array);		
		}
		$j=CJSON::encode($arr);
		echo $j;
	}

	public function actionPast()
	{
		$date=date("Y-m-d");
		$criteria = new CDbCriteria;
		$criteria->limit='4';
		$criteria->condition='date < :date';
		$criteria->params=array(':date' => $date);
		$criteria->order='date DESC';
		$a = Events::model()->findAll($criteria);
		$array=array();
		$arr=array();
		foreach($a as $item)
		{
			$array["id"]=$item->id;
			$array["title"]=$item->title;
			$array["text"]=$item->text;
			$array["text"]=substr($array["text"], 0, 200);
			$array["date"]=$item->date;
			$array["image_url"]=$item->image_url;
			array_push($arr,$array);		
		}
		
		$j=CJSON::encode($arr);
		echo $j;
		
	}
	public function actionPageloader()
	{
		$criteria = new CDbCriteria;
		$startFrom=$_POST['startFrom'];
		$label=$_POST['label'];
		if ($order=0){
			$criteria->order='date DESC';
		} else{
			$criteria->order='date ASC';
		};
		$criteria->limit='4';
		$criteria->offset=$startFrom;
		
		$a = Events::model()->findAll($criteria);
		$array=array();
		$arr=array();
		foreach($a as $item)
		{
			$array["id"]=$item->id;
			$array["title"]=$item->title;
			$array["text"]=$item->text;
			$array["text"]=substr($array["text"], 0, 200);
			$array["date"]=$item->date;
			$array["image_url"]=$item->image_url;
			array_push($arr,$array);		
		}
		
		$j=CJSON::encode($arr);
		echo $j;
		
	}
	
	public function actionVote()
	{
		$votename=$_POST['votename'];
		Voting::model()->updateCounters(array('voices'=>1), 'voter = :votename', array(':votename'=>$votename));
	}

}