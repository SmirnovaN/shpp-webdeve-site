<?php

class LibraryController extends Controller
{
	public function actionIndex()
	{
        $data_array = array();

        /*выборка из таблицы библиотеки*/

        $criteria = new CDbCriteria;
        $criteria->limit='10';



        if(!isset($_POST['data'])){
            $criteria->order='id DESC';
            $send_action='';
        }
        else{
            $send_action=$_POST['data'];

            if ($send_action=='name'){
                $criteria->order='name';
            }
            elseif ($send_action=='free'){
                $criteria->order='free';
            }
            else {
                $criteria->order='id DESC';
            }
        }

        $a = Library::model()->findAll($criteria);

        $array_lib=array();
        $arr_lib=array();
        foreach($a as $item)
        {
            $array_lib["name"]=$item->name;
            $array_lib["data_busy"]=$item->data_busy;
            $array_lib["data_free"]=$item->data_free;
            $array_lib["user_book_busy"]=$item->user_book_busy;
            $array_lib["book_free"]=$item->book_free;
            $array_lib["info_book"]=$item->info_book;
            $array_lib["photo_href"]=$item->photo_href;
            array_push($arr_lib,$array_lib);
        }


        /*сборка в общий массив и его вывод*/

        array_push($data_array,$arr_lib);

        $resulting_array = array(
            array($data_array[0])     // library

        );

        $this->render('index',array('data'=>$resulting_array));




/*		$this->render('index');*/
	}













	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}