<?php

class WebdevController extends Controller
{
	public function actionGetlist()
	{
		$criteria = new CDbCriteria;
		$criteria->limit='8';
		$criteria->order='username ASC';
		$a = User::model()->findAll($criteria);
		$array=array();
		$arr=array();
		foreach($a as $item)
		{
			$array["username"]=$item->username;
			array_push($arr,$array);		
		}
		$j=CJSON::encode($arr);
		echo $j;
	}

	public function actionIndex()
	{
		$this->render('index');
	}
}