<?php

class VotingController extends Controller
{
	public function actionIndex()
	{
		$models = Voting::model()->findAllBySql('SELECT voices FROM voting');
		$this->render('index',array('models'=>$models));
	}
}