<?php

class NewsController extends Controller
{
	public function actionIndex()
	{
		$baseUrl = Yii::app()->baseUrl; 
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/voting.css');
		
		$model = new News_home;
		$resulting_array = $model->getList();
        
        
		
		//$this->render('index',array('data' => $resulting_array));
       
        
        if(isset($_GET['news_id'])){
            
            $model = new News_home;
            $resulting_array = $model->FullPage($_GET['news_id']);
            
            $this->render('full_news',array('alt' => $resulting_array));
            
        }
        else {
            $this->render('index',array('data' => $resulting_array));
        }
        
       
        
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}