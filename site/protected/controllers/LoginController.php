<?php

class LoginController extends Controller
{
	public function actionIndex()
	{
        $model = new LoginForm;
        $model->scenario = 'registration';

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('index',array('model'=>$model));

	}

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegistration()
    {
        $model=new User;
        $model->scenario = 'registration';
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];

            $setting = Setting::model()->findByPk(1);
            if($setting->defaultStatusUser == 0){
            $model->ban = 0;
        } else {
            $model->ban = 1;
        }


            if($model->save()){
                if($setting->defaultStatusUser == 0){
                Yii::app()->user->setFlash('registration','Ожидайте подтверждения админа.');
            } else{
                    Yii::app()->user->setFlash('registration','Вы можете авторизоваться.');
                }
            }

        }

        $this->render('registration',array(
            'model'=>$model,
        ));
    }



	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}