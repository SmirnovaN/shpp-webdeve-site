<?php

class HomeController extends Controller
{
	public function actionIndex()
	{
        $data_array = array();

        /*выборка событий из таблицы новостей*/

        $criteria = new CDbCriteria;
        $criteria->select = 'CONCAT(SUBSTRING(`text`, 1, 140), "...") `text`, `id`, `title`, `date`, `image_thumbnail`';
        $criteria->order = 'id DESC';
        $criteria->limit = '4';
        $a = News_home::model()->findAll($criteria);        //тут изменить название модели на News после доработки новостей
        $array_events = array();
        $arr_events = array();
        foreach($a as $one)
        {
            $array_events['text'] = $one->text;
            $array_events['id'] = $one->id;
            $array_events['title'] = $one->title;
            $array_events['date'] = $one->date;
            $array_events['image_thumbnail'] = $one->image_thumbnail;
            array_push($arr_events,$array_events);
        }

        /*выборка курсов из таблицы курсов*/

        $criteria = new CDbCriteria;
        $criteria->select = "CONCAT(SUBSTRING(`text`, 1, 100), '...') `text`, `id`, `title`, `link`, `image`";
        $criteria->order = 'id DESC';
        $criteria->limit = '4';
        $a = Courses::model()->findAll($criteria);

        $array_courses = array();
        $arr_courses = array();

        foreach($a as $one){
            $array_courses ['text'] = $one->text;
            $array_courses ['id'] = $one->id;
            $array_courses['title']  = $one->title;
            $array_courses['link']  = $one->link;
            $array_courses['image']  = $one->image;
            array_push($arr_courses,$array_courses);
        }

        /*выборка картинок для основного окна слайдера из таблицы новостей*/

        $criteria = new CDbCriteria;
        $criteria->select = "CONCAT(SUBSTRING(`text`, 1, 50), '...') `text`, `id`, `title`, `image_slider`";
        $criteria->order = 'id DESC';
        $criteria->limit = 10;
        $a = News_home::model()->findAll($criteria);    //тут изменить название модели на News после доработки новостей

        $array_slider_window = array();
        $arr_slider_window = array();

        foreach($a as $one){
            $array_slider_window ['text'] = $one->text;
            $array_slider_window ['id'] = $one->id;
            $array_slider_window['title']  = $one->title;
            $array_slider_window['image_slider']  = $one->image_slider;
            array_push($arr_slider_window,$array_slider_window);
        }

        /*выборка картинок для бокового окна слайдера из таблицы новостей*/

        $criteria = new CDbCriteria;
        $criteria->select = "`id`, `image_slider_thumbnail`";
        $criteria->order = 'id DESC';
        $criteria->limit = 10;
        $a = News_home::model()->findAll($criteria);    //тут изменить название модели на News после доработки новостей

        $array_slider_window_thumbnails = array();
        $arr_slider_window_thumbnails = array();

        foreach($a as $one){
            $array_slider_window_thumbnails ['id'] = $one->id;
            $array_slider_window_thumbnails['image_slider_thumbnail']  = $one->image_slider_thumbnail;
            array_push($arr_slider_window_thumbnails,$array_slider_window_thumbnails);
        }

        array_push($data_array,$arr_events,$arr_courses,$arr_slider_window,$arr_slider_window_thumbnails);

        $resulting_array = array(
            array($data_array[0]),     // events
            array($data_array[1]),     // courses
            array($data_array[2]),     // slider_window
            array($data_array[3]),     // slider_window_thumbnails
        );

        $this->render('index',array('data'=>$resulting_array));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}