
<?php
/* @var $this UserController */
/* @var $model User */

echo '';
$this->menu=array(
	array('label'=>'журнал пользователей', 'url'=>array('index')),
	array('label'=>'изменение пользователя', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'удаление пользователя', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены?')),

);

?>
<h1 class="smallcaps">просмотр пользователя <?php echo $model->username; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
        'created',
		'ban',
		'role',
		'email',
	),
)); ?>
