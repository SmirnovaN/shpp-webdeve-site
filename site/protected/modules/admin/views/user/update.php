<?php
/* @var $this UserController */
/* @var $model User */


$this->menu=array(
	array('label'=>'журнал пользователя', 'url'=>array('index')),
	array('label'=>'просмотр пользователя', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'изменить пароль', 'url'=>array('password', 'id'=>$model->id)),
);
?>

<h1 class="smallcaps">Изменение пользователя <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>