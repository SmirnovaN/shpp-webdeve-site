<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<div class="labelwidth"><?php echo $form->label($model,'id'); ?></div>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
        <div class="labelwidth"><?php echo $form->label($model,'username'); ?></div>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
        <div class="labelwidth"><?php echo $form->label($model,'ban'); ?></div>
		<?php echo $form->dropDownList($model,'ban', array(''=>'', 0=>"Да",1=>"Нет")); ?>
	</div>

	<div class="row">
        <div class="labelwidth"><?php echo $form->label($model,'role'); ?></div>
        <?php echo $form->dropDownList($model,'role', array(''=>'', 1=>"User",2=>"Admin")); ?>
	</div>

	<div class="row">
        <div class="labelwidth"><?php echo $form->label($model,'email'); ?></div>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>


        <div class="buttons">
		<?php echo CHtml::submitButton('Найти'); ?>

	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->