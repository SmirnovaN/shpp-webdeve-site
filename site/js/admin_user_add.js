function admin_user_add()
{
    login = $("#admin_user_add_login").val();
    password = $("#admin_user_add_password").val();
    password2 = $("#admin_user_add_password2").val();
    email = $("#admin_user_add_email").val();
    secret_word = $("#admin_user_add_secret_word").val();
    admin = $("#admin_user_add_admin").val();
    admin = ((admin > '1') ? '0' : '1');
    if ((login) && (password) && (password2) && (email) && (secret_word) && (admin))
    {
        if (password == password2)
        {
            $.ajax
            ({
                type: "POST",
                url: "index.php?controller=admin_user&action=admin_add",
                data: {login:login,password:password,email:email,secret_word:secret_word,admin:admin}
            }).done(function( date )
                {
                    if (date)
                    {
                        location.replace("index.php?controller=admin_user&action=admin_index");
                    }
                });
        }
        else
        {
            $("#regist").text("Разные пароли");
        }
    }
    else
    {
        $("#regist").text("Введены не все данные");
    }

}

function admin_user_not()
{
    location.replace("index.php?controller=admin_user&action=admin_index");
}