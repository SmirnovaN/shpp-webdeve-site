admin_select();
function admin_select()
{
    $.ajax
    ({
        type: "POST",
        url: "index.php?controller=admin_user&action=admin_select",
        data: {}
    }).done(function( date )
        {
            $("#admin_user").html(date);
        });
}

function user_edit(id)
{
    $.ajax
    ({
        type: "POST",
        url: "index.php?controller=admin_user&action=admin_edit",
        data: {id:id}
    }).done(function( date )
        {
            $("#admin_user").html(date);
        });

}

function user_edit_ok(id)
{
    login = $("#admin_user_login").val();
    password = $("#admin_user_password").val();
    email = $("#admin_user_email").val();
    secret_word = $("#admin_user_secret_word").val();
    admin = $("#admin_user_admin").val();
    $.ajax
    ({
        type: "POST",
        url: "index.php?controller=admin_user&action=admin_update",
        data: {id:id ,login:login,password:password,email:email,secret_word:secret_word,admin:admin}
    }).done(function( date )
        {
            if (date)
            {
                admin_select();
            }
        });
}

function user_delete(id)
{
    $.ajax
    ({
        type: "POST",
        url: "index.php?controller=admin_user&action=admin_delete",
        data: {id:id}
    }).done(function( date )
        {
            if (date)
            {
                admin_select();
            }
        });
}

function user_edit_no()
{
    admin_select();
}

function admin_user_add()
{
    location.replace("index.php?controller=admin_user&action=admin_add");
}
function admin_news()
{
    location.replace("index.php?controller=check&action=index")
}

