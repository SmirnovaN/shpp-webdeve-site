﻿$(document).ready(function(){
	
	function simple_timer(sec, block, direction){
			var time=sec;
			direction=direction||false;
			var minutes=parseInt(time/60);
			if (minutes<1) minutes=0;
			time=parseInt(time-minutes*60);
			if (minutes<10)minutes='0'+minutes;
			var seconds=time;
			if (seconds<10) seconds='0'+seconds;
		 
			block.innerHTML=minutes+':'+seconds;
		 
			if (direction){
				sec++;
				setTimeout(function(){ simple_timer(sec, block, direction);},1000);
			} else{
				sec--;
			if (sec>0){
					setTimeout(function(){simple_timer(sec, block, direction);},1000);
				} else {
					$('.preview-test').hide();
					$('.test').show();
				}
			}
		}
	$(".clicked-link").click(function(){
			var li_value=$(this).text();
			var str=$("#test-name").text();
			var test_name = str.replace(str,li_value); 
			$("#test-name").text(test_name);		
			$(".test-list").hide();
			$.ajax({
						dataType:'json',
						url: "webdev/getlist"
				   }).done(function(msg){
				  
				   var res='<ul>';
					for (var i=0; i<msg.length; i++){
					var e=msg[i];
						res +=  '<li>' + e.username + '</li>';
					};
					res+='</ul>';
					$( ".online-list" ).html(res);
						   });
			$(".peoplelist").show();
			var block=document.getElementById('countdown');
			simple_timer(5, block);
	});

	$("#results").click(function() {                
			
		var scored = 0;   
		for (var i=0;i<4;i++){
			if($("input[name=q"+i+"]:checked").val() == 1){
				scored++;
			} 
		};
		$("#scored").html(scored);
		$("#third-question").hide();
		$(".score").show();
		
	});
	$("#next1").click(function() {                
			
		if($("input[name=q1]:checked").val()){
			$("#first-question").hide();      
			$("#second-question").show();	
		} 
		else{
			alert("You're not done yet!");  
		};    
		
	});
	$("#next2").click(function() {                
			
		if($("input[name=q2]:checked").val()){
			$("#second-question").hide();      
			$("#third-question").show();	
		} 
		else{
			alert("You're not done yet!");  
		};    
		
	});
}); 