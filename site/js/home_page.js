function get_home_slider_update() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "get_home_slider_update.php",
    }).done(function( data ) {
            $(".amazingslider-slides").html(data);
        });
}
get_home_slider_update();
setInterval(
    get_home_slider_update
    , 1000
);

function get_home_slider_thumbnails_update() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "get_home_slider_thumbnails_update.php",
    }).done(function( data ) {
            $(".amazingslider-thumbnails").html(data);
        });
}
get_home_slider_thumbnails_update();
setInterval(
    get_home_slider_thumbnails_update
    , 1000
);


function get_home_news_update() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "get_home_news_update.php",
    }).done(function( data ) {
            $(".insert-event-main").html(data);
        });
}
get_home_news_update();
setInterval(
    get_home_news_update
    , 100
);

function get_home_courses_update() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "get_home_courses_update.php",
    }).done(function( data ) {
            $(".insert-course-main").html(data);
        });
}
get_home_courses_update();
setInterval(
    get_home_news_update
    , 100
);