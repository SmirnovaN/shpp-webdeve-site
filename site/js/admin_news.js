admin_select();
    function admin_select()
    {
        $.ajax
        ({
            type: "POST",
            url: "index.php?controller=admin_news&action=admin_select",
            data: {}
        }).done(function( date )
                {
                    $("#admin_news").html(date);
                });
    }

    function news_edit(id)
    {
        $.ajax
        ({
            type: "POST",
            url: "index.php?controller=admin_news&action=admin_edit",
            data: {id:id}
        }).done(function( date )
                {
                    $("#admin_news").html(date);
                });

    }

    function news_edit_ok(id)
    {
        image = $("#admin_news_image").val();
        title = $("#admin_news_title").val();
        text = $("#admin_news_text").val();
        author = $("#admin_news_author").val();
        date = $("#admin_news_date").val();
        $.ajax
        ({
            type: "POST",
            url: "index.php?controller=admin_news&action=admin_update",
            data: {id:id ,image:image,title:title,text:text,author:author,date:date}
        }).done(function( date )
                {
                    if (date)
                    {
                        admin_select();
                    }
                });
    }

    function news_delete(id)
    {
        $.ajax
        ({
            type: "POST",
            url: "index.php?controller=admin_news&action=admin_delete",
            data: {id:id}
        }).done(function( date )
                {
                    if (date)
                    {
                        admin_select();
                    }
                });
    }
    function admin_news_add()
    {
        location.replace("index.php?controller=admin_news&action=admin_add");
    }
    function news_edit_no()
    {
        admin_select();
    }
    function admins_user()
    {
        location.replace("index.php?controller=admin_user&action=admin_index");
    }