﻿get_nearest_event();
var inProgress=false;
var startFrom=10;

function more(label){
	$.ajax({
			url: 'events/pageloader',
			type: 'POST',
			dataType:'json',
			data:{startFrom:startFrom, label:label},
			beforeSend:function(){
			inProgress = true;
			}
	}).done(function(msg){
		var res="";
		var id=0;
		if (msg.length > 0){
			for (var i=0; i<msg.length; i++){	
				var e=msg[i];
				id=parseInt(e.id);
				res += "<div class='event' id='e"+id+"'><img src='"+e.image_url+"'><div class='event-date'>"+e.date+"</div><br/><div class='event-title'><b>"+e.title+"</b></div><br/><div class='event-text' id='etext"+id+"'>"+e.text+"<div class='points' id='"+id+"' onClick='resize("+id+");'>...</div></div><br/></div><div class='up' id='up"+id+"'>Up</div>";
				inProgress = false;
				startFrom += 4;
			};

			res +="<button id='more' onClick='more("+label+");'>Дальше!</button>";
			$(".past-events").html(res); 
		} else if (label==0){
			get_past_events();
		} else{
			get_future_events();
		}
	});
};
function get_past_events(){

	$.ajax({
		dataType:'json',
		url: "events/past"
	}).done(function(msg){
		var res="";
		var id=0;
		for (var i=0; i<msg.length; i++){	
			var e=msg[i];
			id=parseInt(e.id);
			res += "<div class='event' id='e"+id+"'><img src='"+e.image_url+"'><div class='event-date'>"+e.date+"</div><br/><div class='event-title'><b>"+e.title+"</b></div><br/><div class='event-text' id='etext"+id+"'>"+e.text+"<div class='points' id='"+id+"' onClick='resize("+id+");'>...</div></div><br/></div><div class='up' id='up"+id+"'>Up</div>";
		};
		var label = 0;
		res +="<button id='more' onClick='more("+label+");'>Дальше!</button>";
		$(".near-event").hide();
		$(".future-events").hide();
		$(".past-events").html(res); 
		$(".past-events").show();	
		
		});		
};	

function get_future_events(){
	$.ajax({
		dataType:'json',
		url: "events/future"
	}).done(function(msg){
		var res="";
		var id=0;
		for (var i=0; i<msg.length; i++){	
			var e=msg[i];
			id=parseInt(e.id);
			res += "<div class='event' id='e"+id+"'><img src='"+e.image_url+"'><div class='event-date'>"+e.date+"</div><br/><div class='event-title'><b>"+e.title+"</b></div><br/><div class='event-text' id='etext"+id+"'>"+e.text+"<div class='points' id='"+id+"' onClick='resize("+id+");'>...</div></div><br/></div><div class='up' id='up"+id+"'>Up</div>";
		};
		var label = 1;
		res +="<button id='more' onClick='more("+label+");'>Дальше!</button>";
		$(".near-event").hide();
		$(".past-events").hide();
		$(".future-events").html(res); 
		$(".future-events").show();	
		
		});				
};
function resize(el_id){
	
	$('#'+el_id+'').hide();
	$('#e'+el_id+'').animate({"height":'300'},700); 
	full(el_id);
	$('#up'+el_id+'').show();
	$('#up'+el_id+'').click(function(){
		$('#up'+el_id+'').hide();
		cut(el_id);
		$('#e'+el_id+'').animate({"height":'110'},700); 				
	});
		
};


function full(id){
	
	$.ajax({
		type: 'POST',
		data:{id:id},
		url: "events/full"
	}).done(function(msg){
		$('#etext'+id+'').text(msg);
	});     
};
function cut(id){
	
	$.ajax({
		type: 'POST',
		data:{id:id},
		url: "events/cut"
	}).done(function(msg){
		$('#etext'+id+'').html(msg+"<div class='points' id='"+id+"' onClick='resize("+id+");'>...</div>");
	});     
};
function vote(){
	var votename=$("input[name=votename]:checked").val();
	$.ajax({
		type: 'POST',
		data:{votename:votename},
		url: "events/vote"
	}).done(function(msg){
		alert("thanks for voting!");
		location.href='voting/index';
	});     
};

function get_nearest_event(){
	$.ajax({
		dataType:'json',
		type: 'POST',
		url: "events/nearfuture"
	}).done(function(msg){
		var res="";
		
		for (var i=0; i<msg.length; i++){
			var e=msg[i];
			res += "<div class='event'><img src='"+e.image_url+"'><div class='event-date'>"+e.date+"</div><br/><div class='event-title'><b>"+e.title+"</b></div><br/><div class='event-text'>"+e.text+"</div><br/></div>";
		};
		var title="<center><h2>Our nearest future event:</h2></center>";
		res=title+res;
		$(".near-event").html(res);    
		
	});		  
};
