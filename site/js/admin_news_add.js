function admin_add_news()
{
    image = $("#add_image").val();
    title = $("#add_title").val();
    text = $("#add_text").val();
    author = $("#add_author").val();
    date = $("#add_date").val();
    if ((image != "") && (title != "")&& (text != "")&& (author != ""))
    {
        $.ajax
        ({
            type: "POST",
            url: "index.php?controller=admin_news&action=admin_add",
            data: {image:image,title:title,text:text,author:author,date:date}
        }).done(function( date )
            {
                if (date)
                {
                    location.replace("index.php?controller=check&action=index");
                }
            });
    }
    else
    {
        $("#news_error").text("Введены не все данные");
    }

}
function admin_add_not()
{
    location.replace("index.php?controller=check&action=index");
}


