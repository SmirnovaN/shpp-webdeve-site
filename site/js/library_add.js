window.onload = function(){
select_book();
$("#login_error").css({'display' : 'none'});


/*---------------------------LOG---------------------------*/
function select_book()
{
    $.ajax
    ({
        type: "POST",
        url: "index.php?controller=library&action=library_free_book",
        data: {}
    }).done(function( date )
        {
            if (date)
            {
                $("#book_name").html(date);
            }
        });
}

function send_book()
{
    user = $("#user_name").val();
    id = $("#option").val();
    date_busy = $("#date_busy").val();
    date_free = $("#date_free").val();
    if ((user != "") && (id != "") &&(date_busy != "") &&(date_free != ""))
    {
        if(date_busy > date_free)
        {
            $("#login_error").html("Введены неправельнын даты").show().css({'color': 'red'});
        }
        else
        {
            $.ajax
            ({
                type: "POST",
                url: "index.php?controller=library&action=user_there",
                data: {id:id , user:user , date_busy:date_busy , date_free:date_free}
            }).done(function( date )
                {
                    if(date)
                    {
                        location.replace("index.php?controller=library&action=library_index");
                    }
                    else
                    {
                        $("#login_error").html("Не правельнынй логин").show().css({'color': 'red'});
                    }
                });
        }
    }
    else
    {
        $("#login_error").html("Введены не все данные").show().css({'color': 'red'});
    }
}

}
/*---------------------------LOG---------------------------*/