﻿google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
			['theme', 'voices'],
			['Webdev',v[0]],
			['Android',v[1]],
			['Seo',v[2]],
			['Beer',v[3]],
			['English',v[4]],
			['Gaming',v[5]]
        ]);
        var options = {
          title: 'Top theme'
        };
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
}